{
  description = "Flake for building an OCI image";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  };

  outputs =
    inputs@{ flake-parts, ... }:
    flake-parts.lib.mkFlake { inherit inputs; } {
      # Declare supported systems ("x86_64-linux", "aarch64-linux", "x86_64-darwin", ...)
      systems = [
        "x86_64-linux"
        "aarch64-linux"
        "x86_64-darwin"
        "aarch64-darwin"
      ];

      # Declare files to import
      imports = [
        # Import the image creation files
        ./nix/base-image
      ];
    };
}
