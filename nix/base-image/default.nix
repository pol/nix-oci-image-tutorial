{ ... }:

{
  perSystem =
    { pkgs, ... }:
    {
      packages = {
        # Nix documentation
        # See: https://nix.dev/tutorials/nixos/building-and-running-docker-images.html
        # See: https://grahamc.com/blog/nix-and-layered-docker-images/
        base-image = pkgs.dockerTools.buildLayeredImage {
          # Container name
          name = "base-image";
          # Container tag
          tag = "latest";

          # Container content (everything that will be included in the image)
          contents = [
            # Declare package names from https://search.nixos.org
            pkgs.php82 # PHP interpreter
            pkgs.caddy # Http server
            pkgs.dockerTools.caCertificates # SSL certificates for Caddy
            pkgs.fakeNss # Required by Caddy

            # Add your custom shell scripts
            (pkgs.callPackage ./shell-commands.nix { }).start-http-server

            # Declare a specific folder to include in the image (here, the PHP application)
            ../../.
          ];

          # Extra commands to run in the container (required for PHP)
          extraCommands = ''
            mkdir -p tmp
            chmod 1777 tmp
          '';

          # Container configuration
          config = {
            # Equivalent of Dockerfile's EXPOSE, expose ports to the host
            # See: https://docs.docker.com/reference/dockerfile/#expose
            ExposedPorts = {
              "80/tcp" = { };
              "443/tcp" = { };
            };

            # Equivalent of Dockerfile's CMD, run when the container starts
            # See: https://docs.docker.com/reference/dockerfile/#cmd
            Entrypoint = [ "start-http-server" ];
            Labels = {
              "org.opencontainers.image.vendor" = "European Commission";
            };
          };
        };
      };
    };
}
