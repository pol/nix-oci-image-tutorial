{ pkgs }:

{
  start-http-server = pkgs.writeShellApplication {
    name = "start-http-server";
    text = ''
      php-fpm -D -y /etc/php-fpm.d/www.conf.default
      caddy run --adapter caddyfile --config ${./Caddyfile}
    '';
  };
}
