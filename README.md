# Nix OCI tutorial

## Requirements

- Nix: `curl --proto '=https' --tlsv1.2 -sSf -L https://install.determinate.systems/nix | sh -s -- install`

## Scenario

## 1. Create and run an OCI base image

- `nix build .#base-image`
- `gunzip -c result | docker load` or (depending on your Docker version/config) `docker load -i result`
- `docker run -ti -p8080:80 base-image:latest` or `docker-compose up`
- Go on http://localhost:8080

## 2. Check if the image is reproducible

- `nix build .#base-image --rebuild --keep-failed`

## 3. Check on AWS

Run the step 1 and 2 on AWS.

## 4. SBOM

- `git checkout add-sbom`
- `nix build .#base-image`
- `gunzip -c result | docker load` or (depending on your Docker version/config) `docker load -i result`
- `docker inspect base-image | jq -r '.[0].Config.Labels["SBOM"]' | jq > sbom.json`

## 5. Add patch in PHP

## 6. Fix CVE-2024-3094

- `git checkout fix-CVE-2024-3094`
